import time
import pandas as pd
from json import dumps
from kafka import KafkaProducer
from argparse import ArgumentParser
import csv  


producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

def sendData(filename, topic):
	train_df = pd.read_csv(filename)
	train_df = train_df.drop("PassengerId", axis=1)
	train_df = train_df.where((pd.notnull(train_df)), [None])
	rows = []

	for index, row in train_df.iterrows():
		row_dict = row.to_dict()
		row_dict["Age"] = int(row_dict["Age"]) if row_dict["Age"] != None else None
		row_dict["Pclass"] = int(row_dict["Pclass"]) if row_dict["Pclass"] != None else None
		row_dict["Survived"] = int(row_dict["Survived"]) if row_dict["Survived"] != None else None
		row_dict["SibSp"] = int(row_dict["SibSp"]) if row_dict["SibSp"] != None else None
		row_dict["Parch"] = int(row_dict["Parch"]) if row_dict["Parch"] != None else None

		rows.append(row_dict)

	# print(topic)
	for i in range(len(rows)):
		print(rows[i])
		producer.send(topic, value=rows[i])
        

if __name__ == '__main__':
	parser = ArgumentParser()
	parser.add_argument('-f', '--file', default="train.csv", type=str, help='file to read data from')
	parser.add_argument('-t', '--topic', default="final-test-final", type=str, help='topic to send data to')
	args = parser.parse_args()
	topic = args.topic
	filename = args.file
	
	sendData(filename, topic)
