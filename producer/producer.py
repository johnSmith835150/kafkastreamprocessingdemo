import time
import pandas as pd
from json import dumps
from kafka import KafkaProducer
from argparse import ArgumentParser


producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092', '192.168.23.142:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

def sendData(filename, topic):
	train_df = pd.read_csv(filename)
	train_df = train_df.drop("PassengerId", axis=1)
	train_df = train_df.where((pd.notnull(train_df)), [None])
	row = train_df.iloc[[0]]
	producer.send(topic, key=bytearray(str(time.time()), "UTF-8"), value=dumps(row.to_dict()))

if __name__ == '__main__':
	parser = ArgumentParser()
	parser.add_argument('-f', '--file', default="train.csv", type=str, help='file to read data from')
	parser.add_argument('-t', '--topic', default="test-new", type=str, help='topic to send data to')
	args = parser.parse_args()
	topic = args.topic
	filename = args.file

