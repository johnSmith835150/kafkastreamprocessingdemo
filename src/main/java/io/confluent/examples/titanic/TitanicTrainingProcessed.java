package io.confluent.examples.titanic;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import static io.confluent.examples.titanic.TitanicStream.serdeProps;

public class TitanicTrainingProcessed {
    public boolean survived;
    public int pClass;
    public boolean sex;
    public int age;
    public int fare;
    public int embarked;
    public int title;
    public boolean isAlone;
    public int ageClass;

    public static final Serializer<TitanicTrainingProcessed> TitanicTrainingProcessedSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<TitanicTrainingProcessed> TitanicTrainingProcessedDeserializer = new JsonPOJODeserializer<>(TitanicTrainingProcessed.class);


    public static Serde<TitanicTrainingProcessed> TitanicTrainingProcessedSerde;

    public TitanicTrainingProcessed() {
//        do nothing
    }

    public TitanicTrainingProcessed(boolean survived, int pClass, boolean sex, int age, int fare, int embarked, int title, boolean isAlone, int ageClass) {
        this.survived = survived;
        this.pClass = pClass;
        this.sex = sex;
        this.age = age;
        this.fare = fare;
        this.embarked = embarked;
        this.title = title;
        this.isAlone = isAlone;
        this.ageClass = ageClass;
    }

    public void setSurvived(boolean survived) {
        this.survived = survived;
    }

    public void setpClass(int pClass) {
        this.pClass = pClass;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }

    public void setEmbarked(int embarked) {
        this.embarked = embarked;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public void setIsAlone(boolean isAlone) {
        this.isAlone = isAlone;
    }

    public void setAgeClass(int ageClass) {
        this.ageClass = ageClass;
    }

    public static void createSerde() {
        if (TitanicTrainingProcessedSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", TitanicTrainingProcessed.class);
        TitanicTrainingProcessedSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", TitanicTrainingProcessed.class);
        TitanicTrainingProcessedDeserializer.configure(serdeProps, false);

        TitanicTrainingProcessedSerde = Serdes.serdeFrom(TitanicTrainingProcessedSerializer, TitanicTrainingProcessedDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        
        json.put("survived", survived);
        json.put("pClass", pClass);
        json.put("sex", sex);
        json.put("age", age);
        json.put("fare", fare);
        json.put("embarked", embarked);
        json.put("title", title);
        json.put("isAlone", isAlone);
        json.put("ageClass", ageClass);

        return json.toString();
    }
}
