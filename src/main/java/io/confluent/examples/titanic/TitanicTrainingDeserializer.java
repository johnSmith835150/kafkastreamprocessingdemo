package io.confluent.examples.titanic;

import com.google.gson.Gson;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.Closeable;
import java.nio.charset.Charset;
import java.util.Map;

public class TitanicTrainingDeserializer implements Closeable, AutoCloseable, Deserializer<TitanicTraining> {
    private static final Charset CHARSET = Charset.forName("UTF-8");
    static private Gson gson = new Gson();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public TitanicTraining deserialize(String topic, byte[] bytes) {
        try {
            // Transform the bytes to String
            String person = new String(bytes);
            System.out.println(topic);
            System.out.println(person);
            // Return the Person object created from the String 'person'
            return gson.fromJson(person, TitanicTraining.class);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error reading bytes", e);
        }
    }

    @Override
    public void close() {

    }
}
