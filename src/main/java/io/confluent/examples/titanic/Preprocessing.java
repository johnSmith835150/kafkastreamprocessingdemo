package io.confluent.examples.titanic;

import avro.shaded.com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.round;

public class Preprocessing{
        public static  ArrayList<String> rareTitles = new ArrayList<String>(Arrays.asList("Mr", "Miss","Mrs",
            "Master"));
    public static Map<String, Integer> titleMapping = ImmutableMap.of("Mr",1, "Miss", 2, "Mrs", 3, "Master", 4, "Rare", 5);
    public static Map<String, Boolean> sexMapping = ImmutableMap.of("female", true, "male",false);
    public static Map<String, Integer> embarkedMapping = ImmutableMap.of("S", 0, "C", 1, "Q", 2);

    public static double accPClassSex10, accPClassSex20, accPClassSex30, accPClassSex11, accPClassSex21, accPClassSex31;
    public static double cntPClassSex10, cntPClassSex20, cntPClassSex30, cntPClassSex11, cntPClassSex21, cntPClassSex31;

    public static long embarkedSCount = 0;
    public static long embarkedCCount = 0;
    public static long embarkedQCount = 0;
    public static String maxEmbarkedClass = "";

    public static double accFare = 0.0;
    public static double fareAvg = 0.0;
    public static int count = 0;

    public static TitanicTrainingProcessed process(TitanicTraining RecordIn){
        boolean sex = sexMapping.get(RecordIn.Sex);

        TitanicTrainingProcessed RecordOut = new TitanicTrainingProcessed();
        RecordOut.setTitle(extractTitle(RecordIn.Name));
        RecordOut.setSex(sex);

        int ageBand = estimateAgeBand(RecordIn.Age, RecordIn.Pclass, sex);
        RecordOut.setAge(ageBand);
        RecordOut.setIsAlone(checkAlone(RecordIn.SibSp, RecordIn.Parch));
        RecordOut.setAgeClass(ageBand*RecordIn.Pclass);
        RecordOut.setEmbarked(estimateEmbarked(RecordIn.Embarked));
        RecordOut.setFare(estimateFare(RecordIn.Fare));

        return RecordOut;
    }
    
    
    private static int extractTitle(String Name){

        String title = "Rare";
        if (Name != null) {
            Pattern p = Pattern.compile(" ([A-Za-z]+)\\.");   // the pattern to search for
            Matcher m = p.matcher(Name);


            // if we find a match, get the group
            if (m.find())
            {
                // we"re only looking for one group, so get it
                title = m.group(1);
                // print the group out for verification
                //System.out.format(""%s"\n", theGroup);
            }
            if (!rareTitles.contains(title)){
                title="Rare";
            }
        }

        int titleMap = titleMapping.get(title);
        return titleMap;
    }

    private static int estimateAge(int pClass, boolean sex) {
        double age = 0;
        switch (pClass) {
            case 1:
                age = (sex) ? accPClassSex11 / cntPClassSex11 : accPClassSex10 / cntPClassSex10;
            case 2:
                age = (sex) ? accPClassSex21 / cntPClassSex21 : accPClassSex20 / cntPClassSex20;
            case 3:
                age = (sex) ? accPClassSex31 / cntPClassSex31 : accPClassSex30 / cntPClassSex30;
            default:
                break;
        }

        return (int) round(age);
    }

    private static void accumlateFare(double fare) {
        accFare += fare;
        count++;
        fareAvg = accFare / count;
    }

    private static void accumlateEmbarked (String embarked){
        if (embarked == "S") {
            embarkedSCount++;
        }
        else if (embarked == "C") {
            embarkedCCount++;
        }
        else {
            embarkedQCount++;
        }

        if (embarkedSCount > embarkedCCount && embarkedSCount > embarkedQCount) {
            maxEmbarkedClass = "S";
        }
        else if (embarkedCCount > embarkedSCount && embarkedCCount > embarkedQCount) {
            maxEmbarkedClass = "C";
        }
        else {
            maxEmbarkedClass = "Q";
        }
    }

    private static void accumlateAge(int age, int pClass, boolean sex) {
        switch (pClass) {
            case 1:
                if (sex) {
                    accPClassSex11 += age;
                    cntPClassSex11++;
                }
                else {
                    accPClassSex10 += age;
                    cntPClassSex10++;
                }
            case 2:
                if (sex) {
                    accPClassSex21 += age;
                    cntPClassSex21++;
                }
                else {
                    accPClassSex20 += age;
                    cntPClassSex20++;
                }
            case 3:
                if (sex) {
                    accPClassSex31 += age;
                    cntPClassSex31++;
                }
                else {
                    accPClassSex30 += age;
                    cntPClassSex30++;
                }
            default:
                break;
        }
    }

    private static int estimateAgeBand(int age, int pClass, boolean sex) {
        int ageBand = 0;

        if (age != 0) {
            accumlateAge(age, pClass, sex);
        }
        else {
            age = estimateAge(pClass, sex);
        }

        if(age > 64) {
            ageBand = 4;
        }
        else if(age > 48) {
            ageBand = 3;
        }
        else if(age > 32) {
            ageBand = 2;
        }
        else if(age > 16) {
            ageBand = 1;
        }
        else {
            ageBand = 0;
        }

        return ageBand;
    }

    private static boolean checkAlone(int sibSp, int parch) {
        return !(sibSp + parch >= 1);
    }

    private static int estimateEmbarked(String embarked) {
        if (embarked == null)
            return embarkedMapping.get(maxEmbarkedClass);

        accumlateEmbarked(embarked);
        return embarkedMapping.get(embarked);
    }

    private static int estimateFare(double fare) {
        int fareBand = 0;

        if (fare <= 0.0)
            fare = fareAvg;
        else
            accumlateFare(fare);

        if(fare > 31) {
            fareBand = 3;
        }
        else if(fare > 14.454) {
            fareBand = 2;
        }
        else if(fare > 7.91) {
            fareBand = 1;
        }
        else {
            fareBand = 0;
        }

        return fareBand;
    }
}