package io.confluent.examples.titanic;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;


public class TitanicStream {
    private static final String inputTopics = "titanic-input";
    private static final String outputTopic = "titanic-output";
    public static Map<String, Object> serdeProps = new HashMap<>();

    public static void main(final String[] args) {
        final Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "simple-sentiment-analysis-app");
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, "sentiment-analysis-example-client");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.23.140:9092 , 192.168.23.141:9092 , 192.168.23.142:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, TitanicTrainSerde.class);
        properties.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        TitanicTrainingProcessed.createSerde();

        final StreamsBuilder builder = new StreamsBuilder();
        process(builder); // all processing happens here
        final KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private static void process(final StreamsBuilder builder) {
        final KStream<String, TitanicTraining> source = builder.stream(inputTopics);

        KStream <String, TitanicTrainingProcessed> classified = source.
                map((k, v) -> {
                    TitanicTrainingProcessed processed = Preprocessing.process(v);
                    return KeyValue.pair(k, processed);
                });

        source.foreach( (k, v) -> System.out.println(v) );
        classified.to(outputTopic, Produced.with(Serdes.String(), TitanicTrainingProcessed.TitanicTrainingProcessedSerde));
    }
}
