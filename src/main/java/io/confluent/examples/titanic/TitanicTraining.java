package io.confluent.examples.titanic;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.io.Serializable;

//import static io.confluent.examples.titanic.TitanicStream.serdeProps;

@JsonRootName("TitanicTraining")
public class TitanicTraining implements Serializable {
    //    public String query;
    public int Survived;
    public int Pclass;
    public String Name;
    public String Sex;
    public int Age;
    public int SibSp;
    public int Parch;
    public String Ticket;
    public double Fare;
    public String Embarked;
    public String Cabin;

    public static final Serializer<TitanicTraining> TitanicTrainingSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<TitanicTraining> TitanicTrainingDeserializer = new JsonPOJODeserializer<>(TitanicTraining.class);


    public static Serde<TitanicTraining> TitanicTrainingSerde;

    public TitanicTraining() {
        // defualt constructor
    }

    public TitanicTraining(int Survived, int Pclass, String Name, String Sex, int Age, int SibSp, int Parch, String Ticket, double Fare, String Embarked, String Cabin) {
        this.Survived = Survived;
        this.Pclass = Pclass;
        this.Name = Name;
        this.Sex = Sex;
        this.Age = Age;
        this.SibSp = SibSp;
        this.Parch = Parch;
        this.Ticket = Ticket;
        this.Fare = Fare;
        this.Embarked = Embarked;
        this.Cabin = Cabin;
    }

//    public static void createSerde() {
//        if (TitanicTrainingSerde != null)
//            return;
//
//        serdeProps.put("JsonPOJOClass", TitanicTraining.class);
//        TitanicTrainingSerializer.configure(serdeProps, false);
//
//        serdeProps.put("JsonPOJOClass", TitanicTraining.class);
//        TitanicTrainingDeserializer.configure(serdeProps, false);
//
//        TitanicTrainingSerde = Serdes.serdeFrom(TitanicTrainingSerializer, TitanicTrainingDeserializer);
//    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("Survived", Survived);
        json.put("Pclass", Pclass);
        json.put("Name", Name);
        json.put("Sex", Sex);
        json.put("Age", Age);
        json.put("SibSp", SibSp);
        json.put("Parch", Parch);
        json.put("Ticket", Ticket);
        json.put("Fare", Fare);
        json.put("Embarked", Embarked);
        json.put("Cabin", Cabin);

        return json.toString();
    }

    public static void main(String[] args) {
        String json = "{\"Fare\": 7.8958, \"Name\": \"Markun, Mr. Johann\", \"Embarked\": \"S\", \"Age\": 33.0, \"Parch\": 0, \"Pclass\": 3, \"Sex\": \"male\", \"Survived\": 0, \"SibSp\": 0, \"Ticket\": \"349257\", \"Cabin\": \"null\"}";
        TitanicTraining test = TitanicTrainingDeserializer.deserialize("habd", json.getBytes());
        System.out.println(test);
    }
}
