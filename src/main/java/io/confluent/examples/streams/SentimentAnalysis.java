package io.confluent.examples.streams;


import org.json.simple.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;


public class SentimentAnalysis {

    public static String getSentiment(String input) throws Exception {
        String url = "http://192.168.23.140:5003/sentiment_analysis?input=" + encodeValue(input);

        Request request = new Request();
        JSONObject response = request.get(url);

        return (String) response.get("sentiment");
    }

    // Method to encode a string value using `UTF-8` encoding scheme
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

//    public static void main(String[] args) throws Exception {
//        System.out.println(SentimentAnalysis.getSentiment("I hate you. you suck. this is terrible"));
//    }
}
