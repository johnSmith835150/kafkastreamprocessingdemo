package io.confluent.examples.streams;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.sql.Timestamp;

import static io.confluent.examples.streams.SocialMediaStreamManipulation.serdeProps;

public class SocialMediaQueryResult {
//    public String query;
    public String text;
    public String[] preprocessedText;
    public String language;
    public String timestamp;
    public String sentiment;

    public static final Serializer<SocialMediaQueryResult> SocialMediaQueryResultSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<SocialMediaQueryResult> SocialMediaQueryResultDeserializer = new JsonPOJODeserializer<>();


    public static Serde<SocialMediaQueryResult> SocialMediaQueryResultSerde;

    public SocialMediaQueryResult(String text, String sentiment) {
        this.language = "en";
        this.text = text;
        this.sentiment = sentiment;
        this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
        this.preprocessedText = ProcessNlp.process(text);
    }

    public static void createSerde() {
        if (SocialMediaQueryResultSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", SocialMediaQueryResult.class);
        SocialMediaQueryResultDeserializer.configure(serdeProps, false);

        SocialMediaQueryResultSerde = Serdes.serdeFrom(SocialMediaQueryResultSerializer, SocialMediaQueryResultDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();

        json.put("language", language);
        json.put("text", text);
        json.put("preprocessedText", preprocessedText);
        json.put("timestamp", timestamp);
        json.put("sentiment", sentiment);

        return json.toString();
    }
}
