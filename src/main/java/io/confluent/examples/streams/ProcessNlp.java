package io.confluent.examples.streams;


import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.SimpleTokenizer;

import java.util.ArrayList;
import java.util.Arrays;


public class ProcessNlp {
    private static PorterStemmer stemmer = new PorterStemmer();
    private static Tokenizer tokenizer = new SimpleTokenizer();

    public static ArrayList<String> removeStopWords(ArrayList<String> words) {
        if (!StopWords.isLower)
            StopWords.lower();

        for(int i = 0; i < words.size(); i++) {
            if (StopWords.stopWordsOfWordnet.contains(words.get(i))) {
                words.remove(i);
            }
        }

        return words;
    }

    public static ArrayList<String> stem(ArrayList<String> words) {
        ArrayList<String> stemmedWords = new ArrayList<String>();

        for (String s : words) {
            stemmedWords.add(stemmer.stem(s));
        }

        return stemmedWords;
    }

    public static ArrayList<String> tokinize(String rawText) {
        String tokens[] = tokenizer.tokenize(rawText);

        return new ArrayList<>(Arrays.asList(tokens));
    }

    public static String removeSpecialCharacters(String rawText) {
        return rawText.replaceAll("[^\\w\\s]","");
    }

    public static String[] process(String rawText) {
        rawText = rawText.toLowerCase();
	    rawText= removeSpecialCharacters(rawText);
        ArrayList<String> tokens = tokinize(rawText);
        ArrayList<String> stemmedTokens = stem(tokens);

        ArrayList<String> processed = removeStopWords(stemmedTokens);

        String[] result = processed.toArray(new String[0]);

        return result;
    }

    public static void main(String[] args) {
        String tokens[] = tokenizer.tokenize("this is sample test because we are zh2na");
        ArrayList<String> stemmedTokens = new ArrayList<String>();

//        transforming all stop words to lowercase
        StopWords.lower();

        for (String s : tokens) {
            stemmedTokens.add(stemmer.stem(s).toLowerCase());
        }

        for(int i = 0; i < stemmedTokens.size(); i++) {
            if (StopWords.stopWordsOfWordnet.contains(stemmedTokens.get(i)) || stemmedTokens.get(i).length() < 3) {
                stemmedTokens.remove(i);
            }
        }

        System.out.println(stemmedTokens);
    }
}
