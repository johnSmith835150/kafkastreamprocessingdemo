package io.confluent.examples.streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.*;


public class SocialMediaStreamManipulation {
    private static final String[] inputTopics = {"twitter-topic", "reddit-topic"};// "youtube-topic"};
    private static final String outputTopic = "result-processed";
    public static Map<String, Object> serdeProps = new HashMap<>();

    public static void main(final String[] args) {
        final Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "simple-sentiment-analysis-app");
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, "sentiment-analysis-example-client");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.23.140:9092 , 192.168.23.141:9092 , 192.168.23.142:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        // creating a serde to parse topic input json
        SocialMediaQueryResult.createSerde();

        final StreamsBuilder builder = new StreamsBuilder();
        process(builder); // all processing happens here
        final KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private static void process(final StreamsBuilder builder) {
        final KStream<String, String> source = builder.stream(Arrays.asList(inputTopics));
        System.out.println("Ay 7agaaaaaaaa");
        KStream <String, SocialMediaQueryResult> classified = source.filter((k, v) -> v.length() >= 30)
                .map((k, v) -> {
                    Random rand = new Random();
                    String sentiment = (rand.nextFloat() > 0.5) ? "pos" : "neg";
                    try {
                        sentiment = SentimentAnalysis.getSentiment(v);
                    }
                    catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    SocialMediaQueryResult result = new SocialMediaQueryResult(v, sentiment);
                    return KeyValue.pair(k, result);
                });

        classified.to(outputTopic, Produced.with(Serdes.String(), SocialMediaQueryResult.SocialMediaQueryResultSerde));
    }
}
