package io.confluent.examples.stanford;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class RowTextSerde implements Serde<RowText> {
    private RowTextSerializer serializer = new RowTextSerializer();
    private RowTextDeserializer deserializer = new RowTextDeserializer();


    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        serializer.configure(configs, isKey);
        deserializer.configure(configs, isKey);
    }

    @Override
    public void close() {
        serializer.close();
        deserializer.close();
    }

    @Override
    public Serializer<RowText> serializer() {
        return serializer;
    }

    @Override
    public Deserializer<RowText> deserializer() {
        return deserializer;
    }



    public static void main(String[] args) {
//        String data = "{\"Fare\": 7.25, \"Name\": \"Braund, Mr. Owen Harris\", \"Embarked\": \"S\", \"Age\": 22.0, \"Parch\": 0, \"Pclass\": 3, \"Sex\": \"male\", \"Survived\": 0, \"SibSp\": 1, \"Ticket\": \"A/5 21171\", \"Cabin\": null}";
//        TitanicTrainSerde serde = new TitanicTrainSerde();
//        TitanicTraining test = serde.deserializer.deserialize("titanic-test", data.getBytes());
//        System.out.println(test);
    }
}
