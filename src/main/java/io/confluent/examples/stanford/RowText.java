package io.confluent.examples.stanford;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.io.Serializable;

//import static io.confluent.examples.titanic.TitanicStream.serdeProps;

@JsonRootName("RowText")
public class RowText implements Serializable {
    //    public String query;
    public int Sentiment;
    public String Text;


    public static final Serializer<RowText> RowTextSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<RowText> RowTextDeserializer = new JsonPOJODeserializer<>(RowText.class);


    public static Serde<RowText> RowTextSerde;

    public RowText() {
        // defualt constructor
    }

    public RowText(int Sentiment,String Text) {
        this.Sentiment = Sentiment;
        this.Text = Text;

    }

//    public static void createSerde() {
//        if (TitanicTrainingSerde != null)
//            return;
//
//        serdeProps.put("JsonPOJOClass", TitanicTraining.class);
//        TitanicTrainingSerializer.configure(serdeProps, false);
//
//        serdeProps.put("JsonPOJOClass", TitanicTraining.class);
//        TitanicTrainingDeserializer.configure(serdeProps, false);
//
//        TitanicTrainingSerde = Serdes.serdeFrom(TitanicTrainingSerializer, TitanicTrainingDeserializer);
//    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("Sentiment", Sentiment);
        json.put("Text", Text);


        return json.toString();
    }

    public static void main(String[] args) {
//        String json = "{\"Fare\": 7.8958, \"Name\": \"Markun, Mr. Johann\", \"Embarked\": \"S\", \"Age\": 33.0, \"Parch\": 0, \"Pclass\": 3, \"Sex\": \"male\", \"Survived\": 0, \"SibSp\": 0, \"Ticket\": \"349257\", \"Cabin\": \"null\"}";
//        TitanicTraining test = TitanicTrainingDeserializer.deserialize("habd", json.getBytes());
//        System.out.println(test);
    }
}
