package io.confluent.examples.stanford;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import static io.confluent.examples.stanford.StanfordStream.serdeProps;

public class PreprocessedText {
    private int Sentiment;
    private String[] Text;


    public static final Serializer<PreprocessedText> PreprocessedTextSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<PreprocessedText> PreprocessedTextDeserializer = new JsonPOJODeserializer<>(PreprocessedText.class);


    public static Serde<PreprocessedText> PreprocessedTextSerde;

    public PreprocessedText() {
//        do nothing
    }

    public PreprocessedText(int Sentiment, String[] Text) {
        this.Sentiment = Sentiment;
        this.Text = Text;
    }

    public void setSentiment(int Sentiment) {
        this.Sentiment = Sentiment;
    }

    public void setText(String[] Text) {
        this.Text = Text;
    }


    public static void createSerde() {
        if (PreprocessedTextSerde != null)
            return;

        serdeProps.put("JsonPOJOClass", PreprocessedText.class);
        PreprocessedTextSerializer.configure(serdeProps, false);

        serdeProps.put("JsonPOJOClass", PreprocessedText.class);
        PreprocessedTextDeserializer.configure(serdeProps, false);

        PreprocessedTextSerde = Serdes.serdeFrom(PreprocessedTextSerializer, PreprocessedTextDeserializer);
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("Sentiment", Sentiment);
        json.put("Text", Text);


        return json.toString();
    }
}
